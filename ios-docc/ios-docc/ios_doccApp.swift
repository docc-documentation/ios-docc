//
//  ios_doccApp.swift
//  ios-docc
//
//  Created by Sathya on 04/03/24.
//

import SwiftUI

@main
struct ios_doccApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
